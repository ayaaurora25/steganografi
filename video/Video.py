from subprocess import call, STDOUT
import cv2
import shutil
import os

from ImageEncoder import *


class Video:
    def __init__(self, filename):
        video = cv2.VideoCapture(filename)
        self.fps = video.get(cv2.CAP_PROP_FPS)
        self.codec = int(video.get(cv2.CAP_PROP_FOURCC))
        self.codec = chr(self.codec & 0xff) + chr((self.codec & 0xff00) >> 8) + chr((self.codec & 0xff0000) >> 16) + chr((self.codec & 0xff000000) >> 24)
        self.frames, self.frame_width, self.frame_height = self.get_frames_and_dimension(video)
        self.total_pixels = self.get_total_pixels(len(self.frames), self.frame_width, self.frame_height)

    '''
    :parameter
    video (VideoCapture)
    :return
    frames and its dimension (tuple of list of frame, frame width, and frame height)
    '''
    def get_frames_and_dimension(self, video):
        frames = []
        frame_width, frame_height = int(video.get(3)), int(video.get(4))
        while video.isOpened():
            ret, frame = video.read()
            if ret:
                frames.append(frame)
            else:
                break
        return frames, frame_width, frame_height

    '''
    :parameter
    total frame (int), frame width (int), frame height (int)
    :return
    total pixels (int)
    '''
    def get_total_pixels(self, total_frame, frame_width, frame_height):
        return total_frame * frame_width * frame_height

if __name__ == "__main__":
    v = Video("res/video.avi")
    print(v.frames[0][0][0], v.frames[0][0][1], v.frames[0][0][2], v.frames[0][0][3], v.frames[0][0][4], v.frames[0][0][5], v.frames[0][0][6], v.frames[0][0][7], v.frames[0][0][8], v.frames[0][0][9], v.frames[0][0][10])
    v.save_frames_as_video("res/video_result.avi")

    v = Video("res/video_result.avi")
    print(v.frames[0][0][0], v.frames[0][0][1], v.frames[0][0][2], v.frames[0][0][3], v.frames[0][0][4], v.frames[0][0][5], v.frames[0][0][6], v.frames[0][0][7], v.frames[0][0][8], v.frames[0][0][9], v.frames[0][0][10])

