from flask import Flask, send_file, render_template, request, redirect, url_for
from werkzeug import secure_filename
import os

from audio import stegano_audio
from video import main as stegano_video

app = Flask(__name__)

file_input_dir = 'static/uploads/in/'
message_dir = 'static/uploads/hid/'
file_output_dir = 'static/uploads/out/'
  
@app.route('/play')
def play():
  return redirect('/static/uploads/out/tett.mp4')

@app.route('/get_file', methods = ['GET'])
def get_file():
  file_dir = request.args.get('file')
  return send_file(file_dir, as_attachment=True)

def handle_video_process(filename, message_name, opt):
  method = opt['mode']
  video = stegano_video

  video_input = file_input_dir + filename
  video_output =  file_output_dir + (opt['saveas-text'] if opt['saveas-text'] != "" else os.path.splitext(filename)[0])
  key = opt['keyword']
  
  print('video_input: ' + video_input)
  print('video_output: ' + video_output)
  print('key: ' + key)  

  if method == 'hide':
    message_input = message_dir + message_name
    mode_encryption = (1 if 'encrypt-option' in opt else 0)
    mode_frame_insertion = opt['frame-option']
    mode_pixel_insertion = opt['pixel-option']
    mode_lsb = opt['method-option']

    print('message_input: ', message_input)
    print('mode_encryption: ', mode_encryption)
    print('mode_frame_insertion: ', mode_frame_insertion)
    print('mode_pixel_insertion: ', mode_pixel_insertion)
    print('mode_lsb: ', mode_lsb)

    hide_result_status = video.encode(video_input, video_output, message_input, mode_encryption, mode_frame_insertion, mode_pixel_insertion, mode_lsb, key)

    if hide_result_status == -1:
      return (-1, -1)
      
  elif  method == 'recover':
    message_output = video_output
    video.decode(video_input, message_output, key)

  return video_output + '.mp4', video_output + '.avi'

def handle_audio_process(filename, message_name, opt):
  method = opt['mode']
  audio = stegano_audio.AudioSteganography()

  file_audio = file_input_dir + filename
  file_result =  file_output_dir + (opt['saveas-text'] if opt['saveas-text'] != "" else os.path.splitext(filename)[0])
  key = opt['keyword']
  
  print('file_audio: ' + file_audio)
  print('file_result: ' + file_result)
  print('key: ' + key)  

  file_result_format = ''
  if method == 'hide':
    file_message = message_dir + message_name
    file_result_format = 'wav'
    sequential = (False if opt['frame-option'] != 0 else True)  
    encrypted = (True if 'encrypt-option' in opt else False)
    hide_result_status = audio.hide_message(file_audio, file_message, file_result, key, encrypted, sequential)

    print('file_message: ' + file_message)
    print('sequential: ', sequential)
    print('encrypted: ', encrypted)

    if hide_result_status == -1:
      return (-1, -1)

  elif  method == 'recover':
    file_result_format = audio.extract_message(file_audio, file_result, key)

  return file_result + '.' + file_result_format, file_result + '.' + file_result_format

def process_file(request):  
  form = request.form
  print(form)
  source = form['source']
  method = form['mode']
  file_result = None

  f = request.files['file']
  file_name = secure_filename(f.filename)
  f.save(file_input_dir + file_name)
  print(type(f))

  i_file_name = None
  if method == 'hide':
    i_f = request.files['i_file']
    i_file_name = secure_filename(i_f.filename)
    i_f.save(message_dir + i_file_name)
    print(type(i_f))

  if source == 'video':
    file_result, file_to_download = handle_video_process(file_name, i_file_name, form)
  elif source == 'audio':
    file_result, file_to_download = handle_audio_process(file_name, i_file_name, form)

  if file_result == -1 and file_to_download == -1:
    return {
    'type': source,
    'method' : method,
    'file' : -1,
    'filename' : -1,
    'download_link' : -1,
    }

  media = {
    'type': source,
    'method' : method,
    'file' : file_result,
    'filename' : os.path.basename(file_to_download),
    'download_link' : file_to_download,
  }

  return media

@app.route('/', methods = ['GET', 'POST'])
def index():
  media = None
  print(request)
  if request.method == 'POST':
    media = process_file(request)
    # if 'file' in request.files and 'i_file' in request.files:
    #   print('AYYY')

  print(media)
  return render_template('index.html', media=media)

if __name__ == '__main__':
  app.run(debug=True, host='127.0.0.1')