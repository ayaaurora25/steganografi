from video.Video import *
from video.VideoDecoder import *
from video.ExtendedVigenereCipher import *
from video.IOManager import *
from video.ImageEncoder import *

class VideoEncoder:

    def __init__(self, video, mode_encryption, mode_frame_insertion, mode_pixel_insertion, mode_lsb, message, key, extension):
        self.VIDEO = video

        # Header information with default value
        self.MODE_ENCRYPTION = mode_encryption
        self.MODE_FRAME_INSERTION = mode_frame_insertion
        self.MODE_PIXEL_INSERTION = mode_pixel_insertion
        self.MODE_LSB = mode_lsb
        self.MESSAGE = message

        self.KEY = key
        self.EXTENSION = extension
        self.BYTES_ORDER = self.get_bytes_order()
        self.FRAME_ORDER = self.get_frame_order()

        self.assert_message_fit_video()

        if self.MODE_ENCRYPTION == MODE_ENCRYPTION_ENCRYPTED:
            cipher = ExtendedVigenereCipher()
            self.MESSAGE = cipher.encode(self.MESSAGE, self.KEY)

    def assert_message_fit_video(self):
        if len(self.MESSAGE) <= 0:
            raise Exception("Message cannot be empty")
        if (len(self.VIDEO.frames) - 1) * self.VIDEO.frame_width * self.VIDEO.frame_height * 3 < 8 * len(self.MESSAGE):
            raise Exception("Message does not fit image")

    def get_bytes_order(self):
        bytes_order = []
        if self.MODE_PIXEL_INSERTION == MODE_PIXEL_INSERTION_RANDOM:
            for i in range(0, self.VIDEO.frame_width * self.VIDEO.frame_height):
                bytes_order.append(i)
            random.seed(self.KEY)
            random.shuffle(bytes_order)

        elif self.MODE_PIXEL_INSERTION == MODE_PIXEL_INSERTION_SEQUENTIAL:
            for i in range(0, self.VIDEO.frame_width * self.VIDEO.frame_height):
                bytes_order.append(i)

        return bytes_order

    def get_frame_order(self):
        frame_order = []
        if self.MODE_FRAME_INSERTION == MODE_FRAME_INSERTION_RANDOM:
            for i in range(1, len(self.VIDEO.frames)):
                frame_order.append(i)
            random.seed(self.KEY)
            random.shuffle(frame_order)
            frame_order.insert(0, 0)

        elif self.MODE_FRAME_INSERTION == MODE_FRAME_INSERTION_SEQUENTIAL:
            for i in range(0, len(self.VIDEO.frames)):
                frame_order.append(i)

        return frame_order

    def encode_header(self):
        header_info = "[" + \
                      str(self.MODE_ENCRYPTION) + "," + \
                      str(self.MODE_FRAME_INSERTION) + "," + \
                      str(self.MODE_PIXEL_INSERTION) + "," + \
                      str(self.MODE_LSB) + "," + \
                      str(len(self.MESSAGE)) + "," + \
                      self.EXTENSION + "]"

        image_encoder = ImageEncoder(self.VIDEO.frames[0], MODE_FRAME_INSERTION_SEQUENTIAL, MODE_LSB_ONE_BIT, header_info, self.KEY)
        self.VIDEO.frames[0] = image_encoder.encode()

    def divide_message_per_frame(self):
        len_message_per_frame = 0
        if self.MODE_LSB == MODE_LSB_ONE_BIT:
            len_message_per_frame = int(self.VIDEO.frame_height * self.VIDEO.frame_width / 8)
        elif self.MODE_LSB == MODE_LSB_TWO_BIT:
            len_message_per_frame = int(self.VIDEO.frame_height * self.VIDEO.frame_width / 16)

        result = []
        partial_message = ""
        for char in self.MESSAGE:
            partial_message = partial_message + char
            if len(partial_message) == len_message_per_frame:
                result.append(partial_message)
                partial_message = ""
        if partial_message != "":
            result.append(partial_message)
        return result

    def encode(self):
        self.encode_header()

        messages = self.divide_message_per_frame()
        for frame_num in range(1, len(self.FRAME_ORDER)):
            image_encoder = ImageEncoder(self.VIDEO.frames[self.FRAME_ORDER[frame_num]], self.MODE_PIXEL_INSERTION, self.MODE_LSB, messages.pop(), self.KEY, self.BYTES_ORDER)
            self.VIDEO.frames[self.FRAME_ORDER[frame_num]] = image_encoder.encode()
            if len(messages) <= 0:
                break

    def save_frames_as_video(self, filename):
        shutil.rmtree("res/temp_frames")
        os.makedirs("res/temp_frames")
        frame_num = 1
        for frame in self.VIDEO.frames:
            image_encoder = ImageEncoder(frame, self.MODE_PIXEL_INSERTION, self.MODE_LSB, self.MESSAGE, self.KEY)
            # image_encoder.normalize_image()
            image_encoder.save_image("res/temp_frames/" + str(frame_num) + ".png")
            frame_num = frame_num + 1

        print(filename)
        call(["ffmpeg",
              "-i", "res/temp_frames/%d.png",
              "-r", str(self.VIDEO.fps),
              "-vcodec", "png",
              "-y", filename,
              ], stdout=open(os.devnull, "w"), stderr=STDOUT)

if __name__ == "__main__":

    filename = "res/inputs/sample_2.png"

    mode_encryption = MODE_ENCRYPTION_ENCRYPTED
    mode_frame_insertion = MODE_FRAME_INSERTION_SEQUENTIAL
    mode_pixel_insertion = MODE_PIXEL_INSERTION_SEQUENTIAL
    mode_lsb = MODE_LSB_ONE_BIT
    message, extension = read_file(filename)
    key = "MANUNITED"

    v = Video("res/video.avi")
    vc = VideoEncoder(v, mode_encryption, mode_frame_insertion, mode_pixel_insertion, mode_lsb, message, key, extension)
    vc.encode()
    vc.save_frames_as_video("res/video_result.mp4")

    v = Video("res/video_result.mp4")
    vc = VideoDecoder(v, key)
    write_file("res/outputs/sample_2", vc.EXTENSION, vc.decode())