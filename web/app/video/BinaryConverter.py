from video.AsciiConverter import *

class BinaryConverter:

    ascii_converter = AsciiConverter()

    def __init__(self):
        pass

    '''
    :parameter
    binary representation (string)
    :return
    number (int)
    '''
    def get_int_from_binary(self, binary):
        return int(binary, 2)

    '''
    :parameter
    binary representation (string)
    :return
    character (char)
    '''
    def get_char_from_binary(self, binary):
        return self.ascii_converter.get_char_from_ascii_index(self.get_int_from_binary(binary))

    '''
    :parameter
    binary representation (list of string)
    :return
    message (string)
    '''
    def get_string_from_binary_array(self, binary_array):
        result = ""
        for binary in binary_array:
            result = result + str(self.get_char_from_binary(binary), "UTF-8")
        return result

    '''
    :parameter
    number (int)
    :return
    binary representation (string)
    '''
    def get_binary_from_int(self, num):
        return "{:08b}".format(num)

    '''
    :parameter
    character (char)
    :return
    binary representation (string)
    '''
    def get_binary_from_char(self, char):
        return self.get_binary_from_int(self.ascii_converter.get_ascii_index_from_char(char))

    '''
    :parameter
    message (string)
    :return
    binary representation (list of string)
    '''
    def get_binary_array_from_string(self, string):
        result = []
        for char in string:
            result.append(self.get_binary_from_char(char))
        return result

    '''
    :parameter
    binary representation (list of string, each has length of 8)
    :return
    serialized binary representation (list of string, each has length of 1)
    '''
    def separate_binary_array(self, binary_array):
        result = []
        for binary in binary_array:
            for bit in binary:
                result.append(bit)
        return result

    '''
    :parameter
    binary representation (list of string, each has length of 1)
    :return
    deserialized binary representation (list of string, each has length of 8)
    '''
    def unify_binary_array(self, binary_array):
        result = []
        binary = ""
        for bit in binary_array:
            binary = binary + bit
            if len(binary) == 8:
                result.append(binary)
                binary = ""
        return result


if __name__ == "__main__":
    bc = BinaryConverter()
    message = "HELLO, WORLD!!!"
    binary_message = bc.get_binary_array_from_string(message)
    print(binary_message)
    plain_message = bc.get_string_from_binary_array(binary_message)
    print(plain_message)

    serialized_binary_message = bc.separate_binary_array(binary_message)
    print(serialized_binary_message)
    deserialized_binary_message = bc.unify_binary_array(serialized_binary_message)
    print(deserialized_binary_message)