from video.ImageDecoder import *
from video.ExtendedVigenereCipher import *

class VideoDecoder:
    def __init__(self, video, key):
        self.VIDEO = video
        self.KEY = key
        self.MODE_ENCRYPTION, self.MODE_FRAME_INSERTION, self.MODE_PIXEL_INSERTION, self.MODE_LSB, self.LENGTH_MESSAGE, self.EXTENSION = self.decode_header()
        self.BYTES_ORDER = self.get_bytes_order()
        self.FRAME_ORDER = self.get_frame_order()

    def decode_header(self):
        image_decoder = ImageDecoder(self.VIDEO.frames[0], MODE_FRAME_INSERTION_SEQUENTIAL, MODE_LSB_ONE_BIT, self.KEY)
        header = image_decoder.decode()
        header_info = ""
        for char in header:
            header_info = header_info + char
            if char == ']':
                break

        header_info = header_info.replace("[", "")
        header_info = header_info.replace("]", "")
        header_info = header_info.split(",")

        return int(header_info[0]), int(header_info[1]), int(header_info[2]), int(header_info[3]), int(header_info[4]), header_info[5]

    def get_bytes_order(self):
        bytes_order = []
        if self.MODE_PIXEL_INSERTION == MODE_PIXEL_INSERTION_RANDOM:
            for i in range(0, self.VIDEO.frame_width * self.VIDEO.frame_height):
                bytes_order.append(i)
            random.seed(self.KEY)
            random.shuffle(bytes_order)

        elif self.MODE_PIXEL_INSERTION == MODE_PIXEL_INSERTION_SEQUENTIAL:
            for i in range(0, self.VIDEO.frame_width * self.VIDEO.frame_height):
                bytes_order.append(i)

        return bytes_order

    def get_frame_order(self):
        frame_order = []
        if self.MODE_FRAME_INSERTION == MODE_FRAME_INSERTION_RANDOM:
            for i in range(1, len(self.VIDEO.frames)):
                frame_order.append(i)
            random.seed(self.KEY)
            random.shuffle(frame_order)
            frame_order.insert(0, 0)

        elif self.MODE_FRAME_INSERTION == MODE_FRAME_INSERTION_SEQUENTIAL:
            for i in range(0, len(self.VIDEO.frames)):
                frame_order.append(i)

        return frame_order

    def decode(self):
        messages = ""
        for frame_num in range(1, len(self.VIDEO.frames)):
            image_decoder = ImageDecoder(self.VIDEO.frames[self.FRAME_ORDER[frame_num]], self.MODE_PIXEL_INSERTION, self.MODE_LSB, self.KEY, self.BYTES_ORDER)
            partial_message = image_decoder.decode()
            messages = messages + partial_message
            if len(messages) >= self.LENGTH_MESSAGE:
                break
        messages = messages[:self.LENGTH_MESSAGE]

        if self.MODE_ENCRYPTION == MODE_ENCRYPTION_ENCRYPTED:
            cipher = ExtendedVigenereCipher()
            messages = cipher.decode(messages, self.KEY)

        return messages